﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RocketMovement : MonoBehaviour {

    Rigidbody rigidbody;
    AudioSource rocketThrust;

    [SerializeField]
    float rotationSpeed = 100f;

    [SerializeField]
    float thrustSpeed = 1f;


    enum State
    {
        alive,
        dying,
        transcending
    }

    [SerializeField]
    State currentState;

    // Use this for initialization
    void Start () {
        rigidbody = this.GetComponent<Rigidbody>();
        rocketThrust = this.GetComponent<AudioSource>();
        currentState = State.alive;
	}
	
	// Update is called once per frame
	void Update () {
        if (currentState == State.alive)
        {
            thrust();
            rotate();
        }
        else
        {
            rocketThrust.Stop();
        }
	}

    /// <summary>
    /// Detect when my rocket collides with something else
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter(Collision collision)
    {
        if (currentState != State.alive) return;

        switch (collision.gameObject.tag)
        {
            case "Friendly":
                //do nothing
                Debug.Log("Friendly collision");
                break;

            case "Obstacle":
                Invoke("die", 1f);
                break;

            case "Finish":
                Invoke("finishLevel", 1f);
                //change scenes
                break;
            default:
                //do nothing.
                Invoke("die", 1f);
                break;
        }
    }

    void die()
    {
        currentState = State.dying;
        Debug.Log("Dead boi");
        SceneManager.LoadScene(0);
    }
    void finishLevel()
    {
        currentState = State.transcending;
        if (SceneManager.GetActiveScene().buildIndex == 0)
            SceneManager.LoadScene(1);
    }

    void rotate()
    {
        rigidbody.freezeRotation = true; //Take manual control of rottion.

        
        float frameRotation = rotationSpeed * Time.deltaTime;
        if (Input.GetKey(KeyCode.A))
        {
            //Rotate left
            
            transform.Rotate(Vector3.forward*frameRotation);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            //Rotate right.
            transform.Rotate(Vector3.back*frameRotation);
        }
        rigidbody.freezeRotation = false; //Let the physics engine resume control over rotation.

    }

    void thrust()
    {
        if (Input.GetKey(KeyCode.W))
        {
            //Thrust forward
            rigidbody.AddRelativeForce(new Vector3(0,thrustSpeed,0));
            if (rocketThrust.isPlaying == false)
            {
                rocketThrust.Play();
            }
        }
        else
        {
            rocketThrust.Stop();
        }
    }
}
