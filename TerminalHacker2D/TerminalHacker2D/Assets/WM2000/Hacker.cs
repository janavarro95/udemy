﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Hacker : MonoBehaviour
{

    enum GameState
    {
        MainMenu,
        LevelSelected,
        WinScreen
    }

    GameState currentState;
    int currentLevel;


    string password;

    List<string> easyPasswords;
    List<string> mediumPasswords;
    List<string> hardPasswords;

    // Use this for initialization
    void Start()
    {
        easyPasswords = new List<string>();
        mediumPasswords = new List<string>();
        hardPasswords = new List<string>();
        easyPasswords.Add("Hello");
        mediumPasswords.Add("Goodbye");
        hardPasswords.Add("Unfortunate");

        greeting("");
        currentState = GameState.MainMenu;
    }
    void greeting(string word)
    {
        
        Terminal.ClearScreen();
        print("Hello Console");
        Terminal.WriteLine("HELLO WORLD! " +word);

        Terminal.WriteLine("Hack something?");

        Terminal.WriteLine("");
        Terminal.WriteLine("Press...");
        Terminal.WriteLine("1 for easy.");
        Terminal.WriteLine("2 for medium.");
        Terminal.WriteLine("3 for hard.");

        Terminal.WriteLine("Press enter for input");
        
    }

    void OnUserInput(string input)
    {
        if (currentState == GameState.MainMenu)
        {
            //Terminal.WriteLine("THE user typed: " + input);
            if (input == "1")
            {
                currentLevel = 1;
                StartGame();
            }
            else if (input == "2")
            {
                currentLevel = 2;
                StartGame();
            }
            else if (input == "3")
            {
                currentLevel = 3;
                StartGame();
            }
            else if (input == "menu")
            {
                greeting("");
            }
            else if (input == "007")
            {
                Terminal.WriteLine("Please enter a level Mr. Bond");
            }
            else
            {
                Terminal.WriteLine("Error: Bad input.");
            }
        }
        if (currentState == GameState.LevelSelected)
        {
            if (input == "menu")
            {
                currentState = GameState.MainMenu;
                greeting("");
            }
            else if(input==password && currentLevel==1)
            {
                Terminal.ClearScreen();
                Terminal.WriteLine("YOU DID IT! PASSWORD CORRECT!");
                currentState = GameState.WinScreen;
                displayWinScreen();
            }

            else if (input == password && currentLevel == 2)
            {
                Terminal.ClearScreen();
                Terminal.WriteLine("YOU DID IT! PASSWORD CORRECT!");
                currentState = GameState.WinScreen;
                displayWinScreen();
            }
            else
            {
                Terminal.WriteLine("Wrong password, try again.");

            }
        }
        if (currentState == GameState.WinScreen)
        {
            if (input == "menu")
            {
                currentState = GameState.MainMenu;
                greeting("");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    void StartGame()
    {
        Terminal.ClearScreen();
        currentState = GameState.LevelSelected;


        if (currentLevel == 1)
        {
            System.Random r = new System.Random();
            int randInt = r.Next(0, easyPasswords.Count);
            password = easyPasswords.ElementAt(randInt);
        }
        else if (currentLevel == 2)
        {
            System.Random r = new System.Random();
            int randInt = r.Next(0, mediumPasswords.Count);
            password = mediumPasswords.ElementAt(randInt);
        }
        else if (currentLevel == 3)
        {

        }
        else
        {
            Terminal.WriteLine("An ERROR Occured.");
        }

        Terminal.WriteLine("Please enter a password! Hint: "+password.Anagram());
    }

    public void displayWinScreen()
    {
        if (currentLevel == 1)
        {
            Terminal.WriteLine(@"
//~~~~~~~~~~~~~~~~~~~~~~~~//
//          Yay!          //
//                        //
// Type menu to return    //
//~~~~~~~~~~~~~~~~~~~~~~~~//
            ");
        }
        else if(currentLevel==2)
        {
            
        }
        else
        {
            Terminal.WriteLine("NANI???");
        }
    }
}
